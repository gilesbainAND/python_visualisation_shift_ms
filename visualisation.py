import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

dataframe = pd.read_csv("hello.csv")


def create_graph(y_axis, x_axis = "Month"):

    plt.title("Social Media " + y_axis +" Figures")
    plt.xlabel(x_axis)
    plt.ylabel(y_axis)
    plot_social_media(plt, "Twitter", y_axis, x_axis)
    plot_social_media(plt, "Facebook", y_axis, x_axis)
    plot_social_media(plt, "LinkedIn", y_axis, x_axis)
    plot_social_media(plt, "Youtube", y_axis, x_axis)
    plot_social_media(plt, "Instagram", y_axis, x_axis)
    plt.legend()
    plt.show()

    return plt.gcf()


def plot_social_media(plt, social_name, y_axis, x_axis):

    filter = dataframe["Platform"] == social_name
    data = dataframe[filter]
    plt.plot(x_axis , y_axis, data=data, label=social_name)


followers_graph = create_graph("New followers")
posts_graph = create_graph("Posts")
profile_graph = create_graph("Profile visits")
link_clicks_graph = create_graph("Link clicks")
likes_graph = create_graph("Likes")
comments_graph = create_graph("Comments")

pp = PdfPages('result.pdf')
pp.savefig(followers_graph)
pp.savefig(posts_graph)
pp.savefig(link_clicks_graph)
pp.savefig(likes_graph)
pp.savefig(comments_graph)
pp.close()





