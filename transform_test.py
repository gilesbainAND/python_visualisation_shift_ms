import unittest

from transform import normalise_numeric_cell


class TestNormaliseNumericCell(unittest.TestCase):
    def test_with_integer(self):
       input_cell = "3"
       expected = "3"
       result = normalise_numeric_cell(input_cell)
       self.assertEqual(expected, result)

    def test_with_comma(self):
       input_cell = "3,123"
       expected = "3123"
       result = normalise_numeric_cell(input_cell)
       self.assertEqual(expected, result)

    def test_with_empty_cell(self):
        input_cell = ""
        expected = ""
        result = normalise_numeric_cell(input_cell)
        self.assertEqual(expected, result)

    def test_with_char(self):
        input_cell = "2.1k"
        expected = "2100"
        result = normalise_numeric_cell(input_cell)
        self.assertEqual(expected, result)

    def test_with_char_and_comma(self):
        input_cell = "0,9k"
        expected = "900"
        result = normalise_numeric_cell(input_cell)
        self.assertEqual(expected, result)

    def test_with_not_available(self):
        input_cell = "n/a"
        expected = ""
        result = normalise_numeric_cell(input_cell)
        self.assertEqual(expected, result)

    def test_with_not_available_caps(self):
        input_cell = "N/A"
        expected = ""
        result = normalise_numeric_cell(input_cell)
        self.assertEqual(expected, result)



if __name__ == '__main__':
    unittest.main()
