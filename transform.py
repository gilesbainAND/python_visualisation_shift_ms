import csv

#with open('social_media_data.csv', 'r') as social_media_csv:
#    reader = csv.reader(social_media_csv)
#    for row in reader:
#        print(row)


def save_file(contents, file_name = "results"):
    file = open(file_name + ".csv", "w")
    writer = csv.writer(file)
    writer.writerows(contents)
    file.close()


def load_file(file_path):
    with open(file_path, 'r') as csv_file:
        reader = csv.reader(csv_file)
        input = []
        for row in reader:
            input.append(row)

        return input

def create_matrix(input_matrix):
    output_matrix = []
    first_input_row = input_matrix.pop(0)
    first_input_row.pop(0)
    column_header = ["Year", "Month", "Platform"]
    column_header.extend(first_input_row)
    output_matrix.append(column_header)
    month_array = ["January", "February", "March", "April", "May", "June", "July"]

    year = get_year(input_matrix)
    month = None
    for row in input_matrix:
        first_column = row[0].strip()
        if first_column in month_array:
            month = first_column
            continue
        if first_column == year:
            continue
        if is_empty(row):
            continue

        line = [year, month, row[0]]
        rest_row = row[1:]
        rest_row_formatted = [normalise_numeric_cell(item) for item in rest_row]
        line.extend(rest_row_formatted)
        output_matrix.append(line)

    return output_matrix

def is_empty(row):

    for item in row:
        if item:
            return False

    return True

def get_year(input_matrix):

    for row in input_matrix:
        first_column = row[0]
        if first_column.isdigit():
            return first_column
    return None

def normalise_numeric_cell(cell: str):
    formatted_cell = cell

    if 'k' in formatted_cell:
        formatted_cell = formatted_cell.replace('k', "")
        formatted_cell = formatted_cell.replace(",", ".")
        formatted_cell = float(formatted_cell) * 1000
        formatted_cell = int(formatted_cell)
        formatted_cell = str(formatted_cell)
    elif formatted_cell.lower() == "n/a":
        formatted_cell = ""
    else:
        formatted_cell = cell.replace(",", "")
    return formatted_cell

social_media_array = ["Facebook", "Twitter", "Instagram", "Youtube", "LinkedIn"]


input_matrix = load_file("social_media_data.csv")
matrix = create_matrix(input_matrix)

save_file(matrix, "hello")